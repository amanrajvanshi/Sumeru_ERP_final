<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Purchase;
use App\Models\Seller;
use Illuminate\Http\Request;
use Validator;
class PurchaseController extends Controller
{
    public function statusupdate(Request $req)
    {
    $id = $req -> id;
    $data = DB::SELECT("SELECT * FROM inventory_purchase_orders where id = $id");
    foreach ($data as $value) {
       
    }
    if($value -> order_status == "Active"){
     $result = DB::table('inventory_purchase_orders') 
        ->where('id', $id)
        ->limit(1) 
        ->update(['order_status' => 'Inactive']); 
    }else{
     $result = DB::table('inventory_purchase_orders') 
        ->where('id', $id)
        ->limit(1) 
        ->update(['order_status' => 'Active']);
    }
    }
    public function deleteorder(Request $req,$id){
       $query = DB::table('inventory_purchase_orders') 
            ->where('id', $id)
            ->limit(1) 
            ->update(['order_status' => 'Inactive']);
        if ($query) {
            session() -> flash('success','Order Deleted!');
            return redirect('admin/ERP/purchaseorder');
        }else{
            session() -> flash('error','Order not Deleted!');
            return redirect('admin/ERP/purchaseorder');
        }
    }
    public function createpurchaseorder(Request $req){
         $valid = Validator::make($req -> all(),[
           'seller' => 'required|not_in:0',
           'invoice' => 'required',
           'date' => 'required',
           'amount' => 'required|numeric',
           'fullamount' => 'required|numeric',
           'invoice_file' => 'required|mimes:pdf|max:512',
           'comment' => 'required|max:200',

           'item_name' => 'required|not_in:0',
           'quantity' => 'required|numeric',
           'price' => 'required|numeric',
           'discount' => 'required|numeric',
      ]);
   // return $req;
      if (!$valid -> passes()) {
        return response() -> json(['status' => 'error',
        'error' => $valid -> errors()]);
      }else{
        // $res = new Purchase;
        // $max = Purchase::orderBy('id', 'DESC')->first();
        // return $max;
        // foreach ($max as $value) {
        //     $id = $value -> id;
        // }
        // return $id;
        // $purchase_order_id = $mid + 1;
        $file = $req -> file('invoice_file'); 
        $file_name = time().'.'.$file->getClientOriginalExtension();
        $data = DB::table('inventory_purchase_orders') -> insert([
             'seller_id' => $req -> post('seller'),
             'invoice_id' => $req -> post('invoice'), 
             'total' => $req -> post('amount'), 
             'order_amount' => $req -> post('fullamount'), 
             'date' => $req -> post('date'),
             'order_status' => 'Pending',
             'invoice' =>  $file_name,
             'comment' => $req -> post('comment'),
             'discount' => $req -> post('discount') 
        ]);
        $data2 = DB::table('purchase_order_materials') -> insert([
            'material_id' => $req -> post('item_name'),
            'purchase_oder_id' => $req -> post('maxid'),
            'material_quantity' => $req -> post('quantity'),
            'material_price' => $req -> post('price')
        ]);
        if ($data && $data2) {
               $file->move('upload', $file_name);
               return response() -> json(['status' => 'success',
               'msg' => 'Purchased Successfully!']);
            }else{
               return response() -> json(['status' => 'error',
                'error' => 'There are some problem with your order!']);
            }
    }
    }

    public function editpurchaseorder(Request $req){
      $valid = Validator::make($req -> all(),[
           'status' => 'required'
      ]);
     // return $req;
      if (!$valid -> passes()) {
        return response() -> json(['status' => 'error',
        'error' => $valid -> errors()]);
      }else{
        $pro_id = $req -> post('pro_id');
        $status = $req -> post('status');
        $result = DB::table('inventory_purchase_orders') 
        ->where('id', $pro_id)
        ->limit(1) 
        ->update(['order_status' => $status]); 
        if ($result) {
            return response() -> json(['status' => 'success',
                'msg' => 'Status updated!']);
        }else{
             return response() -> json(['status' => 'error',
                'error' => 'Status not updated!']);
        }
       }


    }
    public function index(Request $req)
    {
       $res = new Purchase;
       // $res = new Seller;
       $cname = $req -> post('invoice');
       $category_name = $cname; 
       $st = $req -> post('status');
       $status = $st; 
       // return $req;
       if ($category_name == null && $status == null) {
           $data = Purchase::get();
           if ($data) {
               return view('../admin/ERP/searchorder',compact('data'));
           }else{
               return redirect('../admin/FLAT/searchorder');
           }
       }
       else if ($category_name == $cname && $status == null) {
           $data = Purchase::where('invoice_id', 'like', '%' . $cname . '%')->paginate(10);
           if ($data) {
               return view('../admin/ERP/searchorder',compact('data'));
           }else{
               return redirect('../admin/ERP/searchorder');
           }
       }
       else if ($category_name == null && $status == $st) {
           $data = Purchase::where('order_status', $status)->paginate(10);
           if ($data) {
               return view('../admin/ERP/searchorder',compact('data'));
           }else{
               return redirect('../admin/ERP/searchorder');
           }
       }
       else if ($category_name == $cname && $status == $st) {
           $data = Purchase::where('invoice_id', 'like', '%' . $cname . '%')
         ->Where('order_status', $status)
         ->paginate(10);
           if ($data) {
               return view('../admin/ERP/searchorder',compact('data'));
           }else{
               return redirect('../admin/ERP/searchorder');
           }
       }
    }
}
