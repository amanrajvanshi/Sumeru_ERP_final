<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('forgotpassword','forgot_password');
Route::view('setpassword/{id}','setpassword');
Route::post('forgotPassword','App\Http\Controllers\AdminController@forgotPassword');
Route::post('setPassword','App\Http\Controllers\AdminController@setPassword');
Route::post('auth','App\Http\Controllers\AdminController@auth');
Route::group(['middleware' => 'disable_back_btn'],function(){
    Route::group(['middleware' => 'admin_auth'],function(){
        Route::view('admin/ERP/dashboard','admin/ERP/dashboard');
        Route::view('admin/ERP/seller','admin/ERP/seller');
        Route::view('admin/ERP/addseller','admin/ERP/addseller');
        Route::view('admin/ERP/addmaterial','admin/ERP/addmaterial');
        Route::view('admin/ERP/addproducts','admin/ERP/addproducts');
        Route::view('admin/ERP/purchaseorder','admin/ERP/purchaseorder');
        Route::view('admin/ERP/sellerpurchaseorder/{id}','admin/ERP/sellerpurchaseorder');
        Route::view('admin/ERP/createpurchaseorder','admin/ERP/createpurchaseorder');
        Route::view('admin/ERP/viewpurchaseorder/{id}','admin/ERP/viewpurchaseorder');
        Route::view('admin/ERP/editpurchaseorder/{id}','admin/ERP/editpurchaseorder');
        Route::view('admin/ERP/editpurchaseorder','admin/ERP/editpurchaseorder');
        Route::view('admin/ERP/purchaseorder/{id}','admin/ERP/purchaseorder');
        Route::view('admin/ERP/searchresult','admin/ERP/searchresult');
        Route::view('admin/ERP/searchcategory','admin/ERP/searchcategory');
        Route::view('admin/ERP/searchproduct','admin/ERP/searchproduct');
        Route::view('admin/ERP/searchorder','admin/ERP/searchorder');
        Route::view('admin/ERP/materialdashboard','admin/ERP/materialdashboard');
        Route::view('admin/ERP/materialcategory','admin/ERP/materialcategory');
        Route::view('admin/ERP/editsellers/{id}','admin/ERP/editseller');
        Route::view('admin/ERP/productstockrecord/{id}','admin/ERP/productstockrecord');
        Route::view('admin/ERP/editproducts/{id}','admin/ERP/editproducts');
        Route::view('admin/ERP/editmaterials/{id}','admin/ERP/editmaterials');
        Route::view('admin/ERP/materialspage','admin/ERP/materialspage');
        Route::view('admin/ERP/sellerprofile/{id}','admin/ERP/sellerprofile');
        Route::post('admin/ERP/statusupdatematerialcategory','App\Http\Controllers\CategoryController@statusupdate');
        Route::post('admin/ERP/statusupdatematerial','App\Http\Controllers\MaterialController@statusupdate');
        Route::post('admin/ERP/statusupdateseller','App\Http\Controllers\SellerController@statusupdate');
        Route::post('admin/ERP/statusupdatepurcahseorder','App\Http\Controllers\PurchaseController@statusupdate');
        Route::post('admin/ERP/createpurchaseorder','App\Http\Controllers\PurchaseController@createpurchaseorder');
        Route::post('admin/ERP/editpurchaseorder','App\Http\Controllers\PurchaseController@editpurchaseorder');
        Route::post('admin/ERP/addSeller','App\Http\Controllers\SellerController@addSeller');
        Route::get('admin/ERP/sellerdelete/{id}','App\Http\Controllers\SellerController@sellerdelete');
        Route::get('admin/ERP/deletematrial/{id}','App\Http\Controllers\CategoryController@deletematrial');
        Route::post('admin/ERP/addCategory','App\Http\Controllers\CategoryController@addCategory');
        Route::post('admin/ERP/addProduct','App\Http\Controllers\MaterialController@addMaterial');
        Route::get('admin/ERP/deleteproduct/{id}','App\Http\Controllers\MaterialController@deleteproduct');
        Route::get('admin/ERP/deleteorder/{id}','App\Http\Controllers\PurchaseController@deleteorder');
        Route::post('admin/ERP/updateProducts','App\Http\Controllers\MaterialController@updateProducts');
        Route::post('admin/ERP/editCategory','App\Http\Controllers\CategoryController@editCategory');
        Route::post('admin/ERP/search','App\Http\Controllers\SellerController@index');
        Route::post('admin/ERP/searchorder','App\Http\Controllers\PurchaseController@index');
        Route::post('admin/ERP/searchcategory','App\Http\Controllers\CategoryController@index');
        Route::post('admin/ERP/searchproduct','App\Http\Controllers\MaterialController@index');
        Route::post('admin/ERP/editSeller','App\Http\Controllers\SellerController@editSeller');
        Route::view('admin/FLAT/dashboard','admin/FLAT/dashboard');
        Route::view('admin/FLAT/searchflat','admin/FLAT/searchflat');
        Route::view('admin/FLAT/searchflatcategory','admin/FLAT/searchflatcategory');
        Route::view('admin/FLAT/flatdashboard','admin/FLAT/flatdashboard');
        Route::view('admin/FLAT/flatstockcategory','admin/FLAT/flatstockcategory');
        Route::view('admin/FLAT/addflatstockcategory','admin/FLAT/addflatstockcategory');
        Route::view('admin/FLAT/addflats','admin/FLAT/addflats');
        Route::view('admin/FLAT/editflats/{id}','admin/FLAT/editflats');
        Route::view('admin/FLAT/editflatstockcategory/{id}','admin/FLAT/editflatstockcategory');
        Route::view('admin/FLAT/flatstockinventory','admin/FLAT/flatstockinventory');
        Route::post('admin/FLAT/searchflatcategory','App\Http\Controllers\FlatCategoryController@index');
        Route::post('admin/FLAT/searchflat','App\Http\Controllers\FlatController@index');
        Route::post('admin/FLAT/statusupdateflatstockinventory','App\Http\Controllers\FlatController@statusupdate');
        Route::post('admin/FLAT/statusupdateflatstockcategory','App\Http\Controllers\FlatCategoryController@statusupdate');
        Route::post('admin/FLAT/addFlatCategory','App\Http\Controllers\FlatCategoryController@addFlatCategory');
        Route::get('admin/FLAT/deleteflatcategory/{id}','App\Http\Controllers\FlatCategoryController@deleteflatcategory');
        Route::post('admin/FLAT/updateFlatCategory','App\Http\Controllers\FlatCategoryController@updateFlatCategory');
        Route::post('admin/FLAT/addFlat','App\Http\Controllers\FlatController@addFlat');
        Route::get(' admin/FLAT/deleteflat/{id}','App\Http\Controllers\FlatController@deleteFlat');
        Route::post('admin/FLAT/editFlat','App\Http\Controllers\FlatController@editFlat');
        Route::get('admin/logout', function () {
        session()->flush();
        return redirect('/');
    });
    });
});